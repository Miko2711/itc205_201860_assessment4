/**
 *
 */
package hotel.tests;

import java.util.Date;

import org.junit.Test;

import hotel.HotelHelper;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.Guest;
import hotel.entities.Hotel;
import hotel.entities.Room;
import hotel.entities.RoomType;
import hotel.entities.ServiceType;
import junit.framework.TestCase;

/**
 * @author
 *
 */
public class TestCases extends TestCase {
	Hotel h = new Hotel();

	@Test
	public void testAddServiceCharge() {
		try {
			h = HotelHelper.loadHotel();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Guest guest = h.registerGuest("ABC", "xyz", 1234);
		Room room = h.findAvailableRoom(RoomType.DOUBLE, new Date(), 3);
		long confirmationNumber = h.book(room, guest, new Date(), 3, 2,
				new CreditCard(CreditCardType.MASTERCARD, 1, 12));
		h.checkin(confirmationNumber);
		h.findBookingByConfirmationNumber(confirmationNumber);
		double cost = h.addServiceCharge(201, ServiceType.BAR_FRIDGE, 100);
		assertEquals(100.0, cost, 0.0);
		double cost1 = h.addServiceCharge(201, ServiceType.BAR_FRIDGE, 100);
		assertEquals(100.0, cost1, 0.0);

	}

	@Test

	public void testAddServiceChargeAfterCheckout() {
		try {
			h = HotelHelper.loadHotel();

			Guest guest = h.registerGuest("ABC", "xyz", 1234);
			Room room = h.findAvailableRoom(RoomType.DOUBLE, new Date(), 3);
			long confirmationNumber = h.book(room, guest, new Date(), 3, 2,
					new CreditCard(CreditCardType.MASTERCARD, 1, 12));
			h.checkin(confirmationNumber);
			h.findBookingByConfirmationNumber(confirmationNumber);
			h.checkout(201);
			double cost1 = h.addServiceCharge(201, ServiceType.BAR_FRIDGE, 100);
			assertEquals(100.0, cost1, 0.0);
		} catch (Exception e) {
			assertEquals("no booking present for room id 201", e.getMessage());
		}

	}

	@Test
	public void testStayLength() {
		try {
			h = HotelHelper.loadHotel();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Guest guest = h.registerGuest("ABC", "xyz", 1234);
		Room room = h.findAvailableRoom(RoomType.DOUBLE, new Date(), 3);
		long confirmationNumber = h.book(room, guest, new Date(), 1, 2,
				new CreditCard(CreditCardType.MASTERCARD, 1, 12));
		int checkin = h.checkin(confirmationNumber);
		assertEquals(1, checkin, 0.0);

	}

}
